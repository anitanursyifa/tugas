import React, { useState } from "react";
import { View, Text, TextInput, TouchableOpacity, StyleSheet } from "react-native";

const Hitung = () => {
    const [panjang, setPanjang] = useState("");
    const [lebar, setLebar] = useState("");
    const [tinggi, setTinggi] = useState("");
    const [luas, setLuas] = useState("");
    const [volume, setVolume] = useState("");

    const hitungLuas=(panjang, lebar,tinggi)=>{
      const temp = panjang*lebar*tinggi;
      setLuas(temp);
    }

    const hitungVolume=(panjang, lebar, tinggi)=>{
      const temp = 2*(panjang*lebar + panjang*tinggi + lebar*tinggi);
      setVolume(temp);
    }

    return (
      <View style={styles.container}>
        <View style={styles.posTitle}>
          <Text style={styles.title}>Menghitung Luas Permukaan dan Volume Balok</Text>
        </View>

          <View style={styles.contInput}>
            <View style={styles.posInput}>
                <TextInput 
                  style={styles.input}
                  placeholder="Masukkan Panjang"
                  placeholderTextColor="#0066ff"
                  onChangeText={(value)=>setPanjang(value)}
                  value={panjang}
                />
            </View>
            <View style={styles.posInput}>
                <TextInput 
                  style={styles.input}
                  placeholder="Masukkan Lebar"
                  placeholderTextColor="#0066ff"
                  onChangeText={(value)=>setLebar(value)}
                  value={lebar}
                />
            </View>
            <View style={styles.posInput}>
                <TextInput 
                  style={styles.input}
                  placeholder="Masukkan Tinggi"
                  placeholderTextColor="#0066ff"
                  onChangeText={(value)=>setTinggi(value)}
                  value={tinggi}
                />
            </View>
            <View style={styles.posButton}>
                <TouchableOpacity
                  style={styles.button}
                  onPress={()=>{hitungLuas(panjang, lebar,tinggi)}}
                >
                  <Text style={styles.textButton}>Hitung Luas</Text>
                </TouchableOpacity>
            </View>

            <View style={styles.posButton}>
                <TouchableOpacity
                  style={styles.button}
                  onPress={()=>{hitungVolume(panjang, lebar,tinggi)}}
                >
                  <Text style={styles.textButton}>Hitung Volume</Text>
                </TouchableOpacity>
            </View>
          </View>
          
        <View style={styles.posOutput}>
          <Text>Luas Balok </Text>
          <Text style={styles.textOutput}>{luas}</Text>
        </View>

        <View style={styles.posOutput}>
          <Text>Volume Balok </Text>
          <Text style={styles.textOutput}>{volume}</Text>
        </View>
      </View>
    )
};

const styles = StyleSheet.create({
    container:{
      marginTop: 40,
      alignItems:'center'
    },
    posTitle:{
      alignItems: 'center'
    },
    title:{
      fontSize : 18,
      fontWeight : 'bold',
      alignItems:'center'
    },
    contInput:{
      backgroundColor:'pink',
      margin: 20,
      padding: 15,
      borderRadius: 15
    },
    posInput:{
      marginLeft : 20,
      marginRight : 20,
      marginBottom : 10,
      backgroundColor : '#99ccff',
      paddingLeft : 10,
      paddingRight: 10,
    },
    input:{
      height : 30
    },
    posButton:{
      margin: 20,
      alignItems:'center'
    },
    button:{
      borderRadius: 5,
      width: 180,
      height: 30,
      alignItems:'center',
      backgroundColor : '#ccffff',
      justifyContent : 'center'
    },
    textButton:{
      fontWeight: 'bold',
      color: '#0066ff'
    },
    posOutput:{
      alignItems:'center'
    },
    textOutput:{
      fontSize: 30,
      fontWeight: 'bold'
    }
})

export default Hitung;
